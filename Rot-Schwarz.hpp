#include <iostream>
#include <time.h>
using namespace std;
/**
 * Klasse für die Impementierung von Rot-Schwarz-Bäumen. Rot-Schwarz_Bäume sind binäre Suchbäume
 * die bestimmte Eigenschaften erfüllen müssen:
 * 1. Jeder Knoten ist entweder rot oder schwarz.
 * 2. Die Wurzel ist schwarz.
 * 3. Alle NIL-Blätter sind schwarz.
 * 4. Ist ein Knoten rot, dann sind seine beiden Kinder schwarz.
 * 5. Für jeden Knoten gilt: Alle Pfade von v zu einem (von v abstammenden) Blatt enthalten die gleiche Anzahl schwarzer Knoten. (Schwarzhöhe immer gleich)
 */
template <typename T>
class SearchTree;

template <typename T>
class TreeNode
{
	friend class SearchTree<T>;

//Datenkapsel für die Speicherung eingefügter Schlüssel
public:
	TreeNode *parent = nullptr;
	TreeNode *left = nullptr;
	TreeNode *right = nullptr;
	bool black = true;

	const T key;
	// Konstruktor
	TreeNode(const T rootKey) : key(rootKey) {}
	// Destruktor
	virtual ~TreeNode()
	{
		if (left != this)
		{
			delete left;
		}
		if (right != this)
		{
			delete right;
		}
	}

	TreeNode *predecessor();
	TreeNode *successor();
	TreeNode *minimum();
	TreeNode *maximum();

private:
	static TreeNode *search(TreeNode *root, const T key);
};

//Gibt den Vorgänger einer node zurück, falls vorhanden
template <class T>
TreeNode<T> *TreeNode<T>::predecessor()
{
	TreeNode *succ = this;
	TreeNode *desc = this;
	if (succ->left != nullptr)
		return succ->left->maximum();
	desc = succ->parent;
	while (desc != nullptr && succ == desc->left)
	{
		succ = desc;
		desc = desc->parent;
	}
	return desc;
}

//Gibt den Nachfolger einer node zurück, falls vorhanden
template <class T>
TreeNode<T> *TreeNode<T>::successor()
{
	TreeNode *succ = this;
	TreeNode *desc = this;
	if (succ->right != nullptr)
		return succ->right->minimum();
	desc = succ->parent;
	while (desc != nullptr && succ == desc->right)
	{
		succ = desc;
		desc = desc->parent;
	}
	return desc;
}

/**
 * @Funktion zur Bestimmung des kleinsten Schlüssel im Baum
 * 
 * @min Speichert den kleinsten key vom Baum 
 * @return min
 */
template <class T>
TreeNode<T> *TreeNode<T>::minimum()
{
	TreeNode *min = this;
	//Schleife die solange den linken Ast vom Baum abläuft bis sie auf ein Nullpointer trifft, der das Ende vom Baum darstellt
	while (min->left != nullptr)
		min = min->left;
	return min;
}


/**
 * @Funktion zur Bestimmung des größten Schlüssel im Baum
 * 
 * @max Speichert den größten key vom Baum 
 * @return max
 */
template <class T>
TreeNode<T> *TreeNode<T>::maximum()
{
	TreeNode *max = this;
	//Schleife die solange den rechten Ast vom Baum abläuft bis sie auf ein Nullpointer trifft, der das Ende vom Baum darstellt
	while (max->right != nullptr)
		max = max->right;
	return max;
}

template <typename T>
class SearchTree
{
	using Node = TreeNode<T>;

private:
	// Wurzel (im Falle eines leeren Baumes: nullptr)
	Node *root;
	Node *nil;
	//Wichtig für das Löschen von Nodes
	void transplant(const Node *const nodeToReplace, Node *const replacementNode);

public:
	SearchTree();
	virtual ~SearchTree() {}
	void insert(const T key);

	void deleteNode(Node *const node);
	Node *search(const T key);
	void insert_fixup(Node *node);

	Node *minimum() { return root->minimum(); }
	Node *maximum() { return root->maximum(); }

	void print();

	void left_rotate(Node *node);

	void right_rotate(Node *node);

	void BenchmarkRB();

	void print2d(Node *tree, int depth);

};

template <class T>
void SearchTree<T>::print()
{
	print2d(root, 0);
}

//Funktion zur Ausgabe des Baumes
template <class T>
void SearchTree<T>::print2d(Node *tree, int depth)
{
	if (tree == nil)
		return;

	print2d(tree->right, depth + 1);

	for (int i = 0; i < depth; i++)
	{
		cout << "     ";
	}
	cout << "(" << tree->key;
	if(tree->black == true)
		cout << "B";
	else
		cout << "R";
    cout << ")" << endl;

	print2d(tree->left, depth + 1);
}

template <class T>
SearchTree<T>::SearchTree(){
    nil = new Node(0);
    nil->parent = nullptr;
    nil->right = nullptr;
    nil->left = nullptr;
    nil->black = true;
    root = nil;
}

//Wird benötigt um einzelne Teilbäume des Baumes umzuhängen
template <typename T>
void SearchTree<T>::transplant(const Node *const nodeToReplace, Node *const replacement)
{
	if (nodeToReplace->parent == nullptr)
		root = replacement;
	else if (nodeToReplace == nodeToReplace->parent->left)
		nodeToReplace->parent->left = replacement;
	else
		nodeToReplace->parent->right = replacement;
	if (replacement != nullptr)
		replacement->parent = nodeToReplace->parent;
}

template <class T>
void SearchTree<T>::left_rotate(Node *node)
{
	Node *y = node->right;
	node->right = y->left;
	if (y->left != nil)
		y->left->parent = node;
	y->parent = node->parent;
	if (node->parent == nil)
		root = y;
	else if (node == node->parent->left)
		node->parent->left = y;
	else
		node->parent->right = y;
	y->left = node;
	node->parent = y;
}

template <class T>
void SearchTree<T>::right_rotate(Node *node)
{
	Node *y = node->left;
	node->left = y->right;
	if (y->right != nil)
		y->right->parent = node;
	y->parent = node->parent;
	if (node->parent == nil)
		root = y;
	else if (node == node->parent->right)
		node->parent->right = y;
	else
		node->parent->left = y;
	y->right = node;
	node->parent = y;
}

//Funktion zum Einfügen einer neuen Node, sowie zum triggern der Methode insert_fixup
template <class T>
void SearchTree<T>::insert(const T key)
{
	Node *z = new Node(key);
	Node *y = nil;
	Node *x = root;
	//Schleife die durch den Baum läuft und die position der eingefügten Node bestimmt
	while (x != nil)
	{
		y = x;
		if (z->key < x->key)
			x = x->left;
		else
			x = x->right;
	}
	z->parent = y;
	if (y == nil)
		root = z;
	else if (z->key < y->key)
		y->left = z;
	else
		y->right = z;

	z->left = nil;
	z->right = nil;
	z->black = false;
	insert_fixup(z);
}

//Methode zur überprüfung der Bedingungen der Eigenschaften eines Rot-Schwarz Baumes
//Falls eine Eigenschaft verletzt wird, wird dies durch insert_fixup korriegiert
template <class T>
void SearchTree<T>::insert_fixup(Node *z)
{

	Node *y = nil;
	while (z->parent->black == false)
	{
		if (z->parent == z->parent->parent->left)
		{
			y = z->parent->parent->right;
			if (y->black == false)
			{
				z->parent->black = true;
				y->black = true;
				z->parent->parent->black = false;
				z = z->parent->parent;
			}
			else if (z == z->parent->right)
			{
				z = z->parent;
				left_rotate(z);
			}
			else{
                z->parent->black = true;
                z->parent->parent->black = false;
                right_rotate(z->parent->parent);
			}
		}
		else
		{
			y = z->parent->parent->left;
			if (y->black == false)
			{
				z->parent->black = true;
				y->black = true;
				z->parent->parent->black = false;
				z = z->parent->parent;
			}
			else if (z == z->parent->left)
			{
				z = z->parent;
				right_rotate(z);
			}
			else{
                z->parent->black = true;
                z->parent->parent->black = false;
                left_rotate(z->parent->parent);
			}
		}
	}
	root->black = true;
}

template <class T>
TreeNode<T> *SearchTree<T>::search(const T key)
{
	return TreeNode<T>::search(root, key);
}
template <class T>
TreeNode<T> *TreeNode<T>::search(TreeNode *root, const T key)
{
	if (root == nullptr)
		return nullptr;
	if (key == root->key)
		return root;
	if (key < root->key)
		return TreeNode<T>::search(root->left, key);
	else
		return TreeNode<T>::search(root->right, key);
}

/**
 * Funktion zum Testen der Laufzeit vom Rot-Schwarz Baum. 
 * Zahlen im Array werden in der schleife mit 100 multipliziert. Beispiel: 200000 entsprechen
 * 2 millionen Einträge.
 * Jeder Eintrag enspricht einer zufällig generierten Zahl die durch Rand() generiert wird 
 * @param Anzahl 
 * 			defeniert die Groeße des Arrays welches die Menge an eingefügten Einträgen enthält
 * @param anzahl_eintraege
 * 			Array für die Anzahl der einegfügten Schlüssel
 * @param zufallszahl
 * 			Zwischenspeicher für zufallszahlen
 * 
 */
void BenchmarkRB()
{
	srand(time(0));
	SearchTree<int> treeBenchmark;
    int Anzahl = 15;
    int anzahl_eintraege[Anzahl] = {2000, 4000, 6000, 8000, 10000, 12000, 14000, 16000, 18000, 20000, 22000, 24000,
                                26000, 28000, 30000};
    int zufallszahl = 0;
    clock_t time;
    for(int j = 0; j < Anzahl; j++){
        time = clock();
        for(int i = 0; i < anzahl_eintraege[j]*100; i++)
        {
            zufallszahl = rand();
            treeBenchmark.insert(zufallszahl);
        }
        time = clock() - time;
        std::cout << anzahl_eintraege[j] << " Time Insert: " << time << "ms" << std::endl;
        time = clock();
        for(int i = 0; i < anzahl_eintraege[j]*100; i++)
        {
            zufallszahl = rand();
            treeBenchmark.search(zufallszahl);
        }
    time = clock() - time;
    std::cout << anzahl_eintraege[j] << " Time Search: " << time << "ms" << std::endl;
    }
}