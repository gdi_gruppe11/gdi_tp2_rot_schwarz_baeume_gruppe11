//Gruppe 11 Mark Kisker, Zinab Amensoor
#include <iostream>
#include <time.h>
#include "Rot-Schwarz.hpp"
#include "binaryTree.hpp"
using namespace std;

/*
* Menü zum ausführen und Testen der Datenstruktur binaryTree, sowie Rot-Schwarz Bäume
* 
*/

int main(int argc, char **argv)
{
    try{
        int abbruch = 1, eingabe, EingabeInsert, cache, EingabeLoeschen, EingabeSearch;
        SearchTree<int> tree;
        do
        {
            system("CLS");
            cout<<"\n Rot-Schwarz Baum Menue";
            cout<<"\n 1: RB_insert";
            cout<<"\n 2: Display the Tree";
            cout<<"\n 3: Testwerte einfgen";
            cout<<"\n 4: Benchmark RB_tree";
            cout<<"\n 5: Benchmark binary_Tree";
            cout<<"\n 0: Exit";
            cout<<"\n Eingabe: ";
            cin>>eingabe;
            while(cin.fail())
            {
                std::cin.clear();
                std::cin.ignore(256, '\n');
                cin >>eingabe;
            }
            cout << eingabe << std::endl;
            try
            {
                if(eingabe == 1)
                {
                    cout<<"\n Welchen Wert soll eingefuegt werden?";
                    cin>>EingabeInsert;
                    tree.insert(EingabeInsert);
                    cout<<"\n Einfuegen Erfolgreich \n";
                    system("Pause");
                }
                else if(eingabe == 2)
                {
                    tree.print();
                    system("Pause");
                }
                else if(eingabe == 3)
                {
                    //Einfügen von wenigen Testdaten zum testen
                    tree.insert(41);
                    tree.insert(38);
                    tree.insert(31);
                    tree.insert(12);
                    tree.insert(19);
                    tree.insert(8);
                    cout<<"\n Einfuegen Erfolgreich \n";
                    system("Pause");
                }
                else if(eingabe == 4)
                {
                    BenchmarkRB();//Benchmark für die rot-schwarz Bäume
                    system("Pause");
                }
                else if(eingabe == 5)
                {
                    BenchmarkB();//Benchmark für die normalen Binären Suchbäume
                    system("Pause");
                }
                else if(eingabe == 0)
                {
                    cout << "\n Programm wurde beendet" << std::endl;
                    abbruch = 0;
                }
                else
                {
                    break;
                }
            }
            catch(const char* err)
            {
                cout << err << endl;
                system("Pause");
            }
        }
        while(abbruch != 0);
    }
    catch(const char* err)
    {
        cout << err << endl;
    }
    return 0;
}
