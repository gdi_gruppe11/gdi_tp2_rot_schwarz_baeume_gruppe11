#include <iostream>
#include <time.h>
using namespace std;

template<typename T>
class TreeNodeB {
    private:
    TreeNodeB* root = nullptr;
    TreeNodeB* father = nullptr;
    TreeNodeB* left = nullptr;
    TreeNodeB* right = nullptr;
    T key_value;
    public:
    void leftRotate(TreeNodeB* tree, TreeNodeB* x);
    T get_key() {return key_value;};
    TreeNodeB(T key);
    TreeNodeB* searchB(TreeNodeB* tree, T key);
    void insertB(TreeNodeB* tree, TreeNodeB *new_node);
    void inorder_tree_walk(TreeNodeB* tree);
    TreeNodeB* minimum(TreeNodeB* tree);
    TreeNodeB* maximum(TreeNodeB* tree);
    void transplant(TreeNodeB* tree, TreeNodeB* u, TreeNodeB* v);
    void tree_delete(TreeNodeB* tree, TreeNodeB* z);
    void BenchmarkB();
};
//Konstruktur
//Zuweisung des Schlüssels
//Verweis auf Wurzel muss gesetzt werden
template<typename T>
TreeNodeB<T> :: TreeNodeB(T key){
    key_value = key;
    father = this;
    root = this;
}
//Ausgabe des Baumes wie auf Seite 291 beschrieben
template<typename T>
void TreeNodeB<T> :: inorder_tree_walk(TreeNodeB* tree){
    if(tree != nullptr){
        inorder_tree_walk(tree->left);
        std::cout << tree->key_value << std::endl;
        inorder_tree_walk(tree->right);
    }
}
//Insert Methode nach Seite 297
template<typename T>
void TreeNodeB<T> :: insertB(TreeNodeB* tree, TreeNodeB* new_node){
    TreeNodeB* y = nullptr;
    TreeNodeB* x = tree->father;
    while (x != nullptr){
        y = x;
        if (new_node->key_value < x->key_value)
            x = x->left;
        else
            x = x->right;
    }
    new_node->father = y;
    if(y == nullptr)
        tree->father = new_node;
    else if (new_node->key_value < y->key_value)
        y->left = new_node;
    else
        y->right = new_node;
}
//Search Methode Iterativ nach Seite 293
template<typename T>
TreeNodeB<T>* TreeNodeB<T> :: searchB(TreeNodeB* tree, T key){
    while (tree != nullptr && key != tree->key_value){
        if (key < tree->key_value)
            tree = tree->left;
        else
            tree = tree->right;
    }
    return tree;
}
//Minimum Methode nach Seite 294
template <typename T>
TreeNodeB<T>* TreeNodeB<T> :: minimum(TreeNodeB* tree){
    while (tree->left != nullptr)
        tree = tree->left;
    return tree;
}
//Maximum Methode nach Seite 294
template <typename T>
TreeNodeB<T>* TreeNodeB<T> :: maximum(TreeNodeB* tree){
    while (tree->right != nullptr)
        tree = tree->right;
    return tree;
}
//Transplant nach Seite 299
template <typename T>
void TreeNodeB<T> :: transplant(TreeNodeB* tree, TreeNodeB* u, TreeNodeB* v){
    if (u->father == nullptr){
        tree->father = v;
    }
    else if (u == u->father->left){
        u->father->left = v;
    }
    else 
        u->father->right = v;
    if (v != nullptr)
        v->father = u->father;
}
//Tree-Delete nach Seite 299
template<typename T>
void TreeNodeB<T> :: tree_delete(TreeNodeB* tree, TreeNodeB* z){
    if (z->left == nullptr)
        tree->transplant(tree, z, z->right);
    else if (z->right == nullptr)
        tree->transplant(tree, z, z->left);
    else {
        TreeNodeB<T>* y = tree->minimum(z->right);
        if (y->father != z){
            tree->transplant(tree, y, y->right);
            y->right = z->right;
            y->right->father = y;
        }
        tree->transplant(tree, z, y);
        y->left = z->left;
        y->left->father = y;
    }
}

/**
 * Funktion zum Testen der Laufzeit vom BinaryTree. 
 * Zahlen im Array werden in der schleife mit 100 multipliziert. Beispiel: 200000 entsprechen
 * 2 millionen Einträge.
 * Jeder Eintrag enspricht einer zufällig generierten Zahl die durch Rand() generiert wird 
 * @param Anzahl 
 * 			defeniert die Groeße des Arrays welches die Menge an eingefügten Einträgen enthält
 * @param anzahl_eintraege
 * 			Array für die Anzahl der einegfügten Schlüssel
 * @param zufallszahl
 * 			Zwischenspeicher für zufallszahlen
 * 
 */
void BenchmarkB()
{
    srand(time(0));
    TreeNodeB<int>* test = new TreeNodeB<int>(50);
    int Anzahl = 15;
    int anzahl_eintraege[Anzahl] = {2000, 4000, 6000, 8000, 10000, 12000, 14000, 16000, 18000, 20000, 22000, 24000,
                                26000, 28000, 30000};
    int zufallszahl = 0;
    clock_t time;
    for(int j = 0; j < Anzahl; j++){
        time = clock();
        for(int i = 0; i < anzahl_eintraege[j]*100; i++)
        {
            zufallszahl = rand();
            TreeNodeB<int>* test1 = new TreeNodeB<int>(zufallszahl);
            test1->insertB(test, test1);
        }
        time = clock() - time;
        std::cout << anzahl_eintraege[j] << " Time Insert: " << time << "ms" << std::endl;
        time = clock();
        for(int i = 0; i < anzahl_eintraege[j]*100; i++)
        {
            zufallszahl = rand();
            test->searchB(test, zufallszahl);
        }
    time = clock() - time;
    std::cout << anzahl_eintraege[j] << " Time Search: " << time << "ms" << std::endl;
    }
}
